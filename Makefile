init:
	ln -fs `pwd`/vimrc ~/.vimrc
	ln -fs `pwd`/zshrc ~/.zshrc
	ln -fs `pwd`/p10k.zsh ~/.p10k.zsh
sync:
	git pull
	git push
