"""
" vim-plug
""
call plug#begin('~/.vim/plugged')

Plug 'karoliskoncevicius/distilled-vim'
Plug 'ntpeters/vim-better-whitespace'
Plug 'editorconfig/editorconfig-vim'
Plug 'easymotion/vim-easymotion'
Plug 'wakatime/vim-wakatime'

call plug#end()



"""
" Character Encoding
""
set encoding=utf-8
scriptencoding utf-8
set fileencodings=utf-8,big5,gbk,latin1
set fileencoding=utf-8



"""
" Indent
""
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set smarttab
set autoindent

" FileType
au FileType make set noexpandtab
au FileType c,cpp,go,rust,java set tabstop=4 softtabstop=4 shiftwidth=4



"""
" Sane Vim Defaults
""
if !has('nvim')
  set nocompatible
  syntax on

  set autoindent
  set autoread
  set backspace=indent,eol,start
  set belloff=all
  set complete-=i
  set display=lastline
  set formatoptions=tcqj
  set history=10000
  set incsearch
  set laststatus=2
  set ruler
  set sessionoptions-=options
  set showcmd
  set sidescroll=1
  set smarttab
  set ttimeoutlen=50
  set ttyfast
  set viminfo+=!
  set wildmenu
endif



"""
" Miscs
""
syntax on
colorscheme distilled
set nu
set ruler
set nofixendofline
inoremap uh <esc>
" my leader is in the opposite of my control key
let mapleader = "-"
" enable 24-bit colors
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

" Search
set incsearch ignorecase smartcase hlsearch
nmap <silent> <BS> :nohlsearch<CR>

" Speedup
set nocursorcolumn
set nocursorline

" show tabs by tapping <Leader>$
nnoremap <silent> <Leader>$ :set list!<CR>
set listchars=tab:⇥\ ,eol:↵

" mark columns over 80 by Justin Force
" https://stackoverflow.com/questions/2447109/showing-a-different-background-colour-in-vim-past-80-characters
if version >= 703
  highlight ColorColumn ctermbg=235 guibg=#2a3f57
  let &colorcolumn="81,121"
endif



"""
" Plugins
""

" easymotion
if !empty(glob($HOME . "/.vim/plugged/vim-easymotion"))
  map <Leader><Leader>s <Plug>(easymotion-s2)
  map <Leader><Leader>L <Plug>(easymotion-bd-jk)
else
  set relativenumber
endif



" ## added by OPAM user-setup for vim / base ## e81f66bc2f43292e0bd2ab3ea2302aab ## you can edit, but keep this line
let s:opam_share_dir = system("opam config var share")
let s:opam_share_dir = substitute(s:opam_share_dir, '[\r\n]*$', '', '')

let s:opam_configuration = {}

function! OpamConfOcpIndent()
  execute "set rtp^=" . s:opam_share_dir . "/ocp-indent/vim"
endfunction
let s:opam_configuration['ocp-indent'] = function('OpamConfOcpIndent')

function! OpamConfOcpIndex()
  execute "set rtp+=" . s:opam_share_dir . "/ocp-index/vim"
endfunction
let s:opam_configuration['ocp-index'] = function('OpamConfOcpIndex')

function! OpamConfMerlin()
  let l:dir = s:opam_share_dir . "/merlin/vim"
  execute "set rtp+=" . l:dir
endfunction
let s:opam_configuration['merlin'] = function('OpamConfMerlin')

let s:opam_packages = ["ocp-indent", "ocp-index", "merlin"]
let s:opam_available_tools = []
for tool in s:opam_packages
  " Respect package order (merlin should be after ocp-index)
  if isdirectory(s:opam_share_dir . "/" . tool)
    call add(s:opam_available_tools, tool)
    call s:opam_configuration[tool]()
  endif
endfor
" ## end of OPAM user-setup addition for vim / base ## keep this line
" ## added by OPAM user-setup for vim / ocp-indent ## 743c24e65e3307af9c8819cad95b8fa7 ## you can edit, but keep this line
if count(s:opam_available_tools,"ocp-indent") == 0
  source "/Users/caasi/.opam/default/share/ocp-indent/vim/indent/ocaml.vim"
endif
" ## end of OPAM user-setup addition for vim / ocp-indent ## keep this line
